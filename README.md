# Serie 2

## Exercice 4:
> Question 2:

a. **La relation entre musicien et instrument:**

Un musicien peut jouer de plusieurs instruments et un intrument peut etre utilisé par plusieurs musiciens il s'agit donc d'une relation **n:p** multivaluée.

b.  **Representation de la relation:**

On utilisera la strecure ``Set`` pour representer la liste d'instrument dont va jouer chaque musicien.

> Question 3 :

a. **Relation entre professeur et instrument:**

Un professeur enseigne d'un seul element et chaque instrument est enseigné que par un  unique prof. Il s'agit donc là d'une relation **1:1**

b. **Type de rerelation entre Professeur est musicien**

Un Professeur est un musicien qui dispense de cours donc il y'aura une relation d'heritage entre les deux. 

Maintenant pour assurer le mapping de cette relation, on choisit la strategie ``SINGLE_TABLE`` car il n'y a pas qu'une seule classe (**Professeur**) qui etend la classe **Musicien**. De plus, un Professeur n'a qu'un champs en plus par rapport à un musicien donc cette strategie parait la mieux adapté.

> Question 4 :
**La relation entre Orchestre et Musiciens**

Un orchestre est composé de plusieurs musiciens et un musiciens peut jouer dans plusieurs orchestres donc il s'agit d'une relation n:p, une relation multivaluée des deux côtés de la relation.
> Question 5 :
**La relation entre Orchestre et Concert**

Un Concert est donné par un Orchestre et un orchestre donne plusieurs Concert. Il s'agit donc d'une relation 1:n entre les deux classes.

**Execution du buildProgramme()**

```
Concert de l'orchestre le Figaro
Date : Sat Dec 26 19:00:00 CET 2020
Lieu : Théâtre Gérard Philippe (Saint Denis)
MR	Léo 	Hautbois
MR	Gabriel 	Violon
MR	Louis 	Harpe
MRS	Julia 	Flûte
MRS	Ines 	Timbales
MRS	Emma 	Piano
MR	Paul 	Contrebasse
MRS	Léa 	Clarinette
MR	Théo 	Trompette
MR	Jean 	Clarinette

```
