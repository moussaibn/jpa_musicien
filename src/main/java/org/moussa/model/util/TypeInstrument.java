package org.moussa.model.util;

import java.util.Arrays;

public enum TypeInstrument {
    CORDES("cordes"),
    VENT("vent"),
    BOIS("bois"),
    CUIVRE("cuivre"),
    CORDES_PINCEES("cordes pincées"),
    CORDES_FRAPPEES("cordes frappées"),
    PERCUSSION("percussion");

    String label;
    private TypeInstrument(String label){
        this.label=label;
    }
    public static TypeInstrument of(String label){
       /* for (TypeInstrument value:values()) {
            if (value.label.equals(label))
                return value;
        }
        return null;

        */
       return Arrays.stream(values()).
               filter(value -> value.label.equals(label))
               .findFirst().orElseThrow();
    }
}
