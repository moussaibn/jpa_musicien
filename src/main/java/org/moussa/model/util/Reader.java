package org.moussa.model.util;

import org.moussa.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Reader {
    private static Map<String, Instrument> registryInstruments = new HashMap<>();
    private static Map<String, Musicien> registryMusiciens = new HashMap<>();
    public static Map<String, Professeur> registryProfesseurs = new HashMap<>();
    private static Map<String, Orchestre> registryOrchestre = new HashMap<>();


    public static List<Instrument> readInstruments() {
        List<Instrument> instruments = List.of();
        Path fichierInstrument = Path.of("data/instrument.txt");
        try (Stream<String> instrumentLines = Files.newBufferedReader(fichierInstrument).lines()) {
            instruments = instrumentLines
                    .filter(l -> !l.startsWith("#"))
                    .map(lineToInstrument).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //instruments.forEach(System.out::println);
        return instruments;
    }

    public static List<Musicien> readMusiciens() {
        List<Musicien> musiciens = List.of();
        Path fichierMucisien = Path.of("data/musique.txt");
        try (Stream<String> musicienLines = Files.newBufferedReader(fichierMucisien).lines()) {
            musiciens = musicienLines.filter(l -> !l.startsWith("#")).map(lineToMusiciens).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //musiciens.forEach(System.out::println);
        return musiciens;
    }

    public static List<Professeur> readProfesseur() {
        List<Professeur> professeurs = List.of();
        Path fichierProf = Path.of("data/professeur.txt");
        try (Stream<String> professeurLines = Files.newBufferedReader(fichierProf).lines()) {
            professeurs = professeurLines.filter(l -> !l.startsWith("#")).map(lineToProfesseur).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //professeurs.forEach(System.out::println);
        return professeurs;
    }

    public static List<Orchestre> readOrchestre() {
        List<Orchestre> orchestres = new ArrayList<>();
        Orchestre orchestre;
        Path fichierOrchestre = Path.of("data/orchestre.txt");

        try (BufferedReader br = Files.newBufferedReader(fichierOrchestre)) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("#")) {
                    String nom = br.readLine().split("[ ]+")[1];
                    String nomDirecteur = br.readLine().strip().split("[:][ ]+")[1];
                    String[] musiciens = br.readLine().split("[,][ ]");

                    Musicien directeur;
                    if (registryProfesseurs.containsKey(nomDirecteur)) {
                        directeur = registryProfesseurs.get(nomDirecteur);
                    } else {
                        directeur = registryMusiciens.get(nomDirecteur);
                    }
                    orchestre = new Orchestre(nom, directeur);
                    Arrays.stream(musiciens)
                            .map(nomMusicien -> registryMusiciens.get(nomMusicien))
                            .forEach(orchestre::addMusicien);
                    orchestres.add(orchestre);
                    registryOrchestre.put(orchestre.getNom(), orchestre);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //orchestres.forEach(System.out::println);
        return orchestres;
    }

    public static List<Concert> readConcert() {
        List<Concert> concerts = new ArrayList<>();
        Path ficierConcert = Path.of("data/concert.txt");

        try (BufferedReader br = Files.newBufferedReader(ficierConcert)) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("#")) {
                    String chDate = br.readLine().split(" : ")[1];
                    String lieu = br.readLine().strip().split(" : ")[1];
                    String nomOrchestre = br.readLine().split("[ ]+")[1].strip();

                    Orchestre orchestre = registryOrchestre.get(nomOrchestre);

                    String jour =chDate.split("à")[0].strip();
                    int heure= Integer.parseInt(chDate.split("[àh]")[1].strip());
                    DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    Date date = new Date(format.parse(jour).getTime());
                    date.setHours(heure);
                    Concert concert = new Concert(date,lieu,orchestre);
                    concerts.add(concert);
                    //concerts.forEach(System.out::println);
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return concerts;

    }

    static Function<String, Instrument> lineToInstrument = lines -> {

        String[] split = lines.split("[ ]+");
        String nomInstrument = split[0];
        String typeInstrument = split[1];
        TypeInstrument type = TypeInstrument.of(typeInstrument);
        Instrument instrument = new Instrument(nomInstrument, type);
        registryInstruments.put(nomInstrument, instrument);
        return instrument;
    };

    private static Function<String, Musicien> lineToMusiciens = lines -> {
        String[] split = lines.split("[ ]+");
        Civilite civilite = Civilite.of(split[0]);
        String nom = split[1];
        Musicien musicien = new Musicien(nom, civilite);
        registryMusiciens.put(nom, musicien);
        String[] nomInstrument = Arrays.copyOfRange(split, 2, split.length);
        Arrays.stream(nomInstrument)
                .map(nomInst -> registryInstruments.get(nomInst))
                .forEach(musicien::addInstrument);
        return musicien;
    };

    private static Function<String, Professeur> lineToProfesseur = lines -> {
        String[] split = lines.split("[ ]+");
        String nom = split[0];
        String nomInstrument = split[1];
        Musicien musicien = registryMusiciens.get(nom);
        Instrument instrument = registryInstruments.get(nomInstrument);
        Professeur professeur = new Professeur(musicien.getNom(), musicien.getCivilite(), instrument);
        professeur.setInstruments(musicien.getInstruments());
        registryProfesseurs.put(nom, professeur);

        return professeur;
    };
}
