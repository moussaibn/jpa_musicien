package org.moussa.model;


import org.moussa.model.util.Civilite;
import org.moussa.model.util.Reader;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.io.Serializable;
@Entity
@DiscriminatorValue("Prof")
public class Professeur extends Musicien implements Serializable {

    public Instrument getEnseigne() {
        return enseigne;
    }

    public void setEnseigne(Instrument enseigne) {
        this.enseigne = enseigne;
    }

    @OneToOne
    private Instrument enseigne;

    public Professeur() {
    }

    public Professeur(String nom, Civilite civilite,Instrument enseigne) {
        super(nom, civilite);
        this.enseigne = enseigne;
    }


}
