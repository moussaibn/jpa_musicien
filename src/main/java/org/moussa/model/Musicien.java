package org.moussa.model;

import org.moussa.model.util.Civilite;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("Musicien")
public class Musicien implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(length = 80)
    private String nom;

    @Column(length = 20)
    @Enumerated(EnumType.STRING)
    private Civilite civilite;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<Instrument> instruments = new HashSet<>();

    @ManyToMany(mappedBy = "musiciens")
    private Set<Orchestre> orchestres = new HashSet<>();

    public boolean addInstrument(Instrument instrument) {
        return this.instruments.add(instrument);
    }

    public Musicien() {
    }

    public Musicien(String nom, Civilite civilite) {
        this.nom = nom;
        this.civilite = civilite;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Civilite getCivilite() {
        return civilite;
    }

    public void setCivilite(Civilite civilite) {
        this.civilite = civilite;
    }

    public Set<Instrument> getInstruments() {
        return new HashSet<>(instruments);
    }

    public void setInstruments(Set<Instrument> instruments) {
        this.instruments = instruments;
    }

    public Set<Orchestre> getOrchestres() {
        return new HashSet<>(orchestres);
    }

    public void setOrchestres(Set<Orchestre> orchestres) {
        this.orchestres = orchestres;
    }

    @Override
    public String toString() {
        return "Musicien{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", civilite=" + civilite +
                ", instruments=" + instruments +
                '}';
    }
}
