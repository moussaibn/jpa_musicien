package org.moussa.model;

import org.moussa.model.util.Reader;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Orchestre implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @OneToOne
    private Musicien directeur;

    @Column(length = 80)
    private String nom;

    @ManyToMany
    private Set<Musicien> musiciens = new HashSet<>();

    public boolean addMusicien(Musicien musicien) {
        if (Reader.registryProfesseurs.containsKey(musicien.getNom()))
            return this.musiciens.add(Reader.registryProfesseurs.get(musicien.getNom()));
        return this.musiciens.add(musicien);
    }

    public Orchestre() {
    }

    public Orchestre(String nom, Musicien directeur) {
        this.nom = nom;
        this.directeur = directeur;
    }

    public int getId() {
        return id;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Musicien> getMusiciens() {
        return new HashSet<>(musiciens);
    }

    public void setMusiciens(Set<Musicien> musiciens) {
        this.musiciens = musiciens;
    }

    public Set<Instrument> getInstruments() {
        Set<Instrument> instruments = new HashSet<>();
        musiciens.forEach(m -> instruments.addAll(m.getInstruments()));
        return instruments;
    }

    public Musicien getDirecteur() {
        return directeur;
    }

    @Override
    public String toString() {
        return "Orchestre{" +
                "id=" + id +
                ", directeur=" + directeur +
                ", nom='" + nom + '\'' +
                '}';
    }
}
