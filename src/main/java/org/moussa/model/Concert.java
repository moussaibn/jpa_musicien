package org.moussa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Concert implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date date;
    private String lieu;

    @ManyToOne
    private Orchestre orchestre;

    public Concert() {
    }

    public Concert(Date date, String lieu, Orchestre orchestre) {
        this.date = date;
        this.lieu = lieu;
        this.orchestre = orchestre;
    }

    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public Orchestre getOrchestre() {
        return orchestre;
    }

    public void setOrchestre(Orchestre orchestre) {
        this.orchestre = orchestre;
    }

    @Override
    public String toString() {
        return "Concert{" +
                "id=" + id +
                ", date=" + date +
                ", lieu='" + lieu + '\'' +
                ", orchestre=" + orchestre +
                '}';
    }

    public String buildProgramme(){
        String chaine= "Concert de l'orchestre le "+ orchestre.getNom()+"\n"+
        "Date : " + date +"\n"+
        "Lieu : " + lieu + "\n";

        for (Musicien musicien : orchestre.getMusiciens()) {
            Instrument instrument = (Instrument) musicien.getInstruments().toArray()[0];
            chaine += musicien.getCivilite() + "\t" + musicien.getNom() + " \t" + instrument.getNom() +"\n";
        }
        return chaine;
    }
}
