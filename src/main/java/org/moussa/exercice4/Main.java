package org.moussa.exercice4;

import org.moussa.model.*;
import org.moussa.model.util.Reader;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Instrument> instruments = Reader.readInstruments();
        List<Musicien> musiciens = Reader.readMusiciens();
        List<Professeur> professeurs= Reader.readProfesseur();
        musiciens = musiciens.stream().
                filter(m-> !Reader.registryProfesseurs.containsKey(m.getNom()))
                .collect(Collectors.toList());

        List<Orchestre> orchestres = Reader.readOrchestre();
        List<Concert> concerts = Reader.readConcert();

        concerts.forEach(Concert::buildProgramme);

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("tp-jpa");
        System.out.println("emf = " + emf);

        EntityManager entityManager = emf.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        musiciens.forEach(entityManager::persist);
        professeurs.forEach(entityManager::persist);
        orchestres.forEach(entityManager::persist);
        concerts.forEach(entityManager::persist);
        transaction.commit();
        concerts.forEach(concert ->  System.out.println(concert.buildProgramme()));

    }
}
